<?php

class Populate {

	public $filename="products.csv";


	public $host="localhost";
	public $user="root";
	public $password="";
	public $db_name="FunDev16";
	public $con;
	public $json_arrays;
	public function __construct() {

		$this->con=new mysqli($this->host,$this->user,$this->password,$this->db_name);
		if ($this->con->connect_errno) {

			echo "Sorry, this website is experiencing problems.";
			echo "Error: Failed to make a MySQL connection, here is why: \n";
			echo "Errno: " . $mysqli->connect_errno . "\n";
			echo "Error: " . $mysqli->connect_error . "\n";
			exit;


		}
		else {
			/* echo "Succesfull connection to DB made"; */

		}

	}

	function show_all() {
		$sql = "SELECT * FROM Products";
		$result = $this->con->query($sql);
		$count = 0;
		$json_arrays = array();
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				/* echo "ID: " . $row["ProductID"]. " Product Name: " . $row["ProductName"]."<br>"; */
				$json_arrays[] = array("id" => $row["ProductID"], "name" => $row["ProductName"], "url" => $row["Path"]);
				$count++;
			}


		} else {
			echo "0 results";
		}
		$word = "total";
		$final_json = array($word => $count, "products" => $json_arrays);
		header('Content-Type: application/json');
		echo json_encode($final_json,JSON_UNESCAPED_SLASHES);
		/* echo json_encode($data); */
		/* echo $json; */

		/* return $json; */
	}

	function read_file() {
		$row = 1;
		$numer = 0;
		if (($handle = fopen($this->filename, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				echo "<p> $num fields in line $row: <br /></p>\n";
				$row++;
				for ($c=0; $c < $num; $c++) {
					echo $data[$c] . "<br />\n";

				}
				$numer++;
			}
			fclose($handle);
		}
	}

}

$test = new Populate();
$test->show_all();

?>
